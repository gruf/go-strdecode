module codeberg.org/gruf/go-strdecode

go 1.18

require (
	codeberg.org/gruf/go-bytesize v0.2.0
	codeberg.org/gruf/go-byteutil v1.0.1
)
