package strdecode

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-byteutil"
)

var (
	// precalculated reflect types for comparisons.
	timeType     = reflect.TypeOf(time.Time{})
	durationType = reflect.TypeOf(time.Duration(0))
	bytesizeType = reflect.TypeOf(bytesize.Size(0))
)

// Unmarshaler represents a type capable of being unmarshaled from a simple string representation.
type Unmarshaler interface {
	UnmarshalString(string) error
}

// Unmarshal ...
func Unmarshal(dst interface{}, src string) error {
	return (&Decoder{}).Unmarshal(dst, src)
}

// Decoder ...
type Decoder struct{ Splitter }

// Unmarshal ...
func (d *Decoder) Unmarshal(dst interface{}, src string) error {
	rv, rt := reflect.ValueOf(dst), reflect.TypeOf(dst)
	if rt.Kind() != reflect.Pointer || rv.IsNil() {
		return errors.New("dst must be non-nil ptr type")
	}
	return d.unmarshal(rv, rt, src)
}

// UnmarshalInto ...
func (d *Decoder) UnmarshalInto(dst reflect.Value, typ reflect.Type, src string) error {
	if !dst.CanInterface() {
		return errors.New("dst must be exported field")
	}
	return d.unmarshal(dst, typ, src)
}

// unmarshal will recursively unmarshal contents of src into receiver dst, handling primitives, predefined reflect tpyes and slices of the above.
func (d *Decoder) unmarshal(dst reflect.Value, typ reflect.Type, src string) error {
	if u, ok := dst.Interface().(Unmarshaler); ok {
		// This implements our Unmarshaler{} interface
		return u.UnmarshalString(src)
	}

	switch {
	// This is a time.Time{}
	case typ == timeType:
		t, err := time.Parse("Jan 2 2006 15:04", src)
		if err != nil {
			return err
		}
		next := reflect.ValueOf(t)
		dst.Set(next)
		return nil
	// This is a time.Duration
	case typ == durationType:
		d, err := time.ParseDuration(src)
		if err != nil {
			return err
		}
		dst.SetInt(int64(d))
		return nil
	// This is a bytesize.Size
	case typ == bytesizeType:
		s, err := bytesize.ParseSize(src)
		if err != nil {
			return err
		}
		dst.SetUint(uint64(s))
		return nil
	}

	switch dst.Kind() {
	// String types
	case reflect.String:
		dst.SetString(src)
		return nil
	// Boolean types
	case reflect.Bool:
		b, err := strconv.ParseBool(src)
		if err != nil {
			return err
		}
		dst.SetBool(b)
		return nil
	// Signed integer types
	case reflect.Int:
		i, err := strconv.ParseInt(src, 10, strconv.IntSize)
		if err != nil {
			return err
		}
		dst.SetInt(i)
		return nil
	case reflect.Int8:
		i, err := strconv.ParseInt(src, 10, 8)
		if err != nil {
			return err
		}
		dst.SetInt(i)
		return nil
	case reflect.Int16:
		i, err := strconv.ParseInt(src, 10, 16)
		if err != nil {
			return err
		}
		dst.SetInt(i)
		return nil
	case reflect.Int32:
		i, err := strconv.ParseInt(src, 10, 32)
		if err != nil {
			return err
		}
		dst.SetInt(i)
		return nil
	case reflect.Int64:
		i, err := strconv.ParseInt(src, 10, 64)
		if err != nil {
			return err
		}
		dst.SetInt(i)
		return nil
	// Unsigned integer types
	case reflect.Uint:
		u, err := strconv.ParseUint(src, 10, strconv.IntSize)
		if err != nil {
			return err
		}
		dst.SetUint(u)
		return nil
	case reflect.Uint8:
		u, err := strconv.ParseUint(src, 10, 8)
		if err != nil {
			return err
		}
		dst.SetUint(u)
		return nil
	case reflect.Uint16:
		u, err := strconv.ParseUint(src, 10, 16)
		if err != nil {
			return err
		}
		dst.SetUint(u)
		return nil
	case reflect.Uint32:
		u, err := strconv.ParseUint(src, 10, 32)
		if err != nil {
			return err
		}
		dst.SetUint(u)
		return nil
	case reflect.Uint64:
		u, err := strconv.ParseUint(src, 10, 64)
		if err != nil {
			return err
		}
		dst.SetUint(u)
		return nil
	// Float number types
	case reflect.Float32:
		f, err := strconv.ParseFloat(src, 32)
		if err != nil {
			return err
		}
		dst.SetFloat(f)
		return nil
	case reflect.Float64:
		f, err := strconv.ParseFloat(src, 64)
		if err != nil {
			return err
		}
		dst.SetFloat(f)
		return nil
	// Complex number types
	case reflect.Complex64:
		c, err := strconv.ParseComplex(src, 64)
		if err != nil {
			return err
		}
		dst.SetComplex(c)
		return nil
	case reflect.Complex128:
		c, err := strconv.ParseComplex(src, 128)
		if err != nil {
			return err
		}
		dst.SetComplex(c)
		return nil
	// Slice types
	case reflect.Slice:
		// Get slice element type
		telem := dst.Type().Elem()
		// Handle special case of a byte slice
		if telem.Kind() == reflect.Uint8 {
			dst.SetBytes(byteutil.S2B(src))
			return nil
		}
		// Reslice starting at 0
		next := dst.Slice(0, 0)
		if err := d.Splitter.SplitFunc(src, func(src string) error {
			// Allocate new element
			elem := reflect.New(telem)
			elem = elem.Elem()
			// Unmarshal slice element into new value
			if err := d.unmarshal(elem, telem, src); err != nil {
				return err
			}
			// Append next element value
			next = reflect.Append(next, elem)
			return nil
		}); err != nil {
			return err
		}
		// Set return slice
		dst.Set(next)
		return nil
	// Pointer types
	case reflect.Pointer:
		// Try get ptr value
		next := dst.Elem()
		// Deref our type
		typ = typ.Elem()
		if !next.IsValid() {
			// Allocate new value at ptr
			dst.Set(reflect.New(typ))
			next = dst.Elem()
		}
		// Go to next ptr layer
		return d.unmarshal(next, typ, src)
	// Ignore all others
	default:
		return fmt.Errorf("invalid receiving type %q", typ.String())
	}
}
