package strdecode_test

import (
	"encoding/json"
	"reflect"
	"strconv"
	"testing"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-strdecode"
)

func TestUnmarshalTime(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect time.Time
	}{
		{
			input:  `Jan 1 1970 00:00`,
			error:  ``,
			expect: mustparsetime(`Jan 1 1970 00:00`),
		},
		{
			input:  `Oct 10 1984 11:30`,
			error:  ``,
			expect: mustparsetime(`Oct 10 1984 11:30`),
		},
		{
			input:  `Jul 27 2049 00:01`,
			error:  ``,
			expect: mustparsetime(`Jul 27 2049 00:01`),
		},
		{
			input: `July 27th 2049 00:01am`,
			error: func() string {
				_, err := time.Parse("Jan 2 2006 15:04", "July 27th 2049 00:01am")
				return err.Error()
			}(),
			expect: time.Time{},
		},
		{
			input: `12:01am 27/07/2049`,
			error: func() string {
				_, err := time.Parse("Jan 2 2006 15:04", "12:01am 27/07/2049")
				return err.Error()
			}(),
			expect: time.Time{},
		},
	})
}

func TestUnmarshalDuration(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect time.Duration
	}{
		{
			input:  `1s`,
			error:  ``,
			expect: time.Second,
		},
		{
			input:  `1m`,
			error:  ``,
			expect: time.Minute,
		},
		{
			input:  `1h`,
			error:  ``,
			expect: time.Hour,
		},
		{
			input:  `1h1m100s`,
			error:  ``,
			expect: time.Hour + time.Minute + 100*time.Second,
		},
		{
			input: `1d`,
			error: func() string {
				_, err := time.ParseDuration(`1d`)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalByteSize(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect bytesize.Size
	}{
		{
			input:  `1kiB`,
			error:  ``,
			expect: 1024,
		},
		{
			input:  `1MiB`,
			error:  ``,
			expect: 1024 * 1024,
		},
		{
			input:  `1MB`,
			error:  ``,
			expect: 1000 * 1000,
		},
		{
			input: `1mb`,
			error: func() string {
				_, err := bytesize.ParseSize(`1mb`)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalString(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect string
	}{
		{
			input:  `hello world`,
			error:  ``,
			expect: `hello world`,
		},
	})
}

func TestUnmarshalBool(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect bool
	}{
		{
			input:  `false`,
			error:  ``,
			expect: false,
		},
		{
			input:  `true`,
			error:  ``,
			expect: true,
		},
		{
			input:  `0`,
			error:  ``,
			expect: false,
		},
		{
			input:  `1`,
			error:  ``,
			expect: true,
		},
		{
			input:  `FALSE`,
			error:  ``,
			expect: false,
		},
		{
			input:  `TRUE`,
			error:  ``,
			expect: true,
		},
		{
			input:  `f`,
			error:  ``,
			expect: false,
		},
		{
			input:  `t`,
			error:  ``,
			expect: true,
		},
		{
			input:  `F`,
			error:  ``,
			expect: false,
		},
		{
			input:  `T`,
			error:  ``,
			expect: true,
		},
		{
			input: `badfalse`,
			error: func() string {
				_, err := strconv.ParseBool("badfalse")
				return err.Error()
			}(),
			expect: false,
		},
		{
			input: `badtrue`,
			error: func() string {
				_, err := strconv.ParseBool("badtrue")
				return err.Error()
			}(),
			expect: false,
		},
	})
}

func TestUnmarshalInt(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect int
	}{
		{
			input:  `0`,
			error:  ``,
			expect: 0,
		},
		{
			input:  `-1`,
			error:  ``,
			expect: -1,
		},
		{
			input:  `+1`,
			error:  ``,
			expect: 1,
		},
		{
			input:  `99999`,
			error:  ``,
			expect: 99999,
		},
		{
			input:  `-99999`,
			error:  ``,
			expect: -99999,
		},
		{
			input: `99,999`,
			error: func() string {
				_, err := strconv.ParseInt("99,999", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `-99,999`,
			error: func() string {
				_, err := strconv.ParseInt("-99,999", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `numberwang`,
			error: func() string {
				_, err := strconv.ParseInt("numberwang", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalUint(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect uint
	}{
		{
			input:  `0`,
			error:  ``,
			expect: 0,
		},
		{
			input:  `1`,
			error:  ``,
			expect: 1,
		},
		{
			input:  `99999`,
			error:  ``,
			expect: 99999,
		},
		{
			input: `99,999`,
			error: func() string {
				_, err := strconv.ParseUint("99,999", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `-99999`,
			error: func() string {
				_, err := strconv.ParseUint("-99999", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `-99,999`,
			error: func() string {
				_, err := strconv.ParseUint("-99,999", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `numberwang`,
			error: func() string {
				_, err := strconv.ParseUint("numberwang", 10, strconv.IntSize)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalFloat(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect float64
	}{
		{
			input:  `0`,
			error:  ``,
			expect: 0,
		},
		{
			input:  `1`,
			error:  ``,
			expect: 1,
		},
		{
			input:  `1.111`,
			error:  ``,
			expect: 1.111,
		},
		{
			input:  `-1.111`,
			error:  ``,
			expect: -1.111,
		},
		{
			input:  `1e10`,
			error:  ``,
			expect: 1e10,
		},
		{
			input: `99-999`,
			error: func() string {
				_, err := strconv.ParseFloat("99-999", 64)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `wumbernang`,
			error: func() string {
				_, err := strconv.ParseFloat("wumbernang", 64)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalComplex(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect complex64
	}{
		{
			input:  `0`,
			error:  ``,
			expect: 0,
		},
		{
			input:  `1`,
			error:  ``,
			expect: 1,
		},
		{
			input:  `1+1i`,
			error:  ``,
			expect: 1 + 1i,
		},
		{
			input:  `-1i`,
			error:  ``,
			expect: -1i,
		},
		{
			input:  `1e10+2i`,
			error:  ``,
			expect: 1e10 + 2i,
		},
		{
			input: `2ii`,
			error: func() string {
				_, err := strconv.ParseComplex("2ii", 64)
				return err.Error()
			}(),
			expect: 0,
		},
		{
			input: `1+2j`,
			error: func() string {
				_, err := strconv.ParseComplex("1+2j", 64)
				return err.Error()
			}(),
			expect: 0,
		},
	})
}

func TestUnmarshalUnmarshaler(t *testing.T) {
	testUnmarshal(t, []struct {
		input  string
		error  string
		expect jsonunmarshaler
	}{
		{
			input: `{"key": "value", "int": 1, "bool": true, "null": null}`,
			error: ``,
			expect: jsonunmarshaler{
				"key":  "value",
				"int":  1.0,
				"bool": true,
				"null": nil,
			},
		},
		{
			input: `{"slice": [1, 2, "three", 4, true]}`,
			error: ``,
			expect: jsonunmarshaler{"slice": []interface{}{
				1.0, 2.0, "three", 4.0, true,
			}},
		},
		{
			input:  `null`,
			error:  ``,
			expect: nil,
		},
		{
			input: `{"slice": [1, 2, "three", 4, true,]}`,
			error: func() string {
				var into jsonunmarshaler
				return json.Unmarshal([]byte(`{"slice": [1, 2, "three", 4, true,]}`), &into).Error()
			}(),
			expect: nil,
		},
		{
			input: `[]`,
			error: func() string {
				var into jsonunmarshaler
				return json.Unmarshal([]byte(`[]`), &into).Error()
			}(),
			expect: nil,
		},
	})
}

func testUnmarshal[Type any](t *testing.T, tests []struct {
	input  string
	error  string
	expect Type
},
) {
	t.Helper() // mark as helper func
	for _, test := range tests {
		var into Type

		t.Logf("type=%T input=\"%s\"", into, test.input)

		if err := strdecode.Unmarshal(&into, test.input); // nocollapse
		(err == nil && test.error != "") || (err != nil && err.Error() != test.error) {
			t.Errorf("error return not as expected: result=\"%v\" expect=\"%v\"", err, test.error)
			continue
		}

		if !reflect.DeepEqual(into, test.expect) {
			t.Errorf("decoded value not as expected: result=\"%#v\" expect=\"%#v\"", into, test.expect)
			continue
		}
	}
}

func mustparsetime(s string) time.Time {
	t, err := time.Parse("Jan 2 2006 15:04", s)
	if err != nil {
		panic(err)
	}
	return t
}

type jsonunmarshaler map[string]interface{}

func (t *jsonunmarshaler) UnmarshalString(s string) error {
	return json.Unmarshal([]byte(s), t)
}
